﻿using Data;
using Data.Models;
using Microsoft.AspNetCore.Mvc;
using Services;
using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestProject.Mapping;

namespace TestProject.Controllers
{
    public class MatchesController : Controller
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly FastScoreContext context;

        public MatchesController(IUnitOfWork unitOfWork, FastScoreContext context)
        {
            this.unitOfWork = unitOfWork;
            this.context = context;
        }


        public async Task<IActionResult> GetAllWithOdds(int sportId, DateTime date)
        {
            var leaguesMatches = await this.unitOfWork.MatchesService.GetAllWithOdds(sportId, date);

            if (!leaguesMatches.Any())
            {
                var sport = await this.unitOfWork.SportsService.GetByIdAsync(sportId);

                return PartialView("_NoMatchFound", model: sport.PhotoUrl);
            }

            var leaguesMatchesVM = ViewModelMapper.Map(leaguesMatches);

            return PartialView("_GetMatches", leaguesMatchesVM);
        }

        public async Task<IActionResult> GetAll(int sportId, DateTime date)
        {
            var leaguesMatches = await this.unitOfWork.MatchesService.GetAll(sportId, date);

            if (!leaguesMatches.Any())
            {
                return PartialView("_NoMatchFound", model: sportId);
            }

            var leaguesMatchesVM = ViewModelMapper.Map(leaguesMatches);

            return PartialView("_GetMatches", leaguesMatchesVM);
        }

        public async Task<IActionResult> GetByDate(int sportId, DateTime date)
        {
            var matchesByDate = await unitOfWork.MatchesService.GetByDate(sportId, date);

            if (!matchesByDate.Any())
            {
                var sport = await this.unitOfWork.SportsService.GetByIdAsync(sportId);

                return PartialView("_NoMatchFound", sport.PhotoUrl);
            }

            var matchesByDateVM = ViewModelMapper.Map(matchesByDate);

            return PartialView("_GetMatches", matchesByDateVM);
        }

        public async Task<IActionResult> GetByMatchStatusId(int sportId, int matchStatusId, DateTime date)
        {
            var matchesByMatchStatusId = await unitOfWork.MatchesService.GetByMatchStatusId(sportId, matchStatusId, date);

            if (!matchesByMatchStatusId.Any())
            {
                var sport = await this.unitOfWork.SportsService.GetByIdAsync(sportId);

                return PartialView("_NoMatchFound", sport.PhotoUrl);
            }

            var matchesByMatchStatus = ViewModelMapper.Map(matchesByMatchStatusId);

            return PartialView("_GetMatches", matchesByMatchStatus);
        }
    }
}
