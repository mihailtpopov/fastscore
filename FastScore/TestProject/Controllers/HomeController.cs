﻿using Consts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services;
using Services.Contracts;
using Services.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using TestProject.Mapping;
using TestProject.Models;

namespace TestProject.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public HomeController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [AllowAnonymous]
        public async Task<IActionResult> Index()
        {
            var matchesByDate = await unitOfWork.MatchesService.GetByDate(SportIdsConsts.FOOTBALL_ID, DateTime.Today);

            if (!matchesByDate.Any())
            {
                var sport = await this.unitOfWork.SportsService.GetByIdAsync(SportIdsConsts.FOOTBALL_ID);

                return View("_NoMatchFound", sport.PhotoUrl);
            }

            var matchesByDateVM = ViewModelMapper.Map(matchesByDate);



            return View("_GetMatches", matchesByDateVM);
        }
    }
}
