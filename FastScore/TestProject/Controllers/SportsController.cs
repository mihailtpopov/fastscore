﻿using Microsoft.AspNetCore.Mvc;
using Services;
using Services.Contracts;
using Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestProject.Controllers
{
    public class SportsController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public SportsController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<IActionResult> GetAll()
        {
            var allSports = await unitOfWork.SportsService.GetAll();
            return View(allSports);
        }
    }
}
