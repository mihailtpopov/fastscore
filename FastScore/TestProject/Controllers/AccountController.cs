﻿using Consts;
using Data;
using Data.Models;
using Data.Models.FilteringModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Services;
using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using TestProject.Mapping;
using TestProject.Models;
using TestProject.ViewModels;
using TestProject.ViewModels.UserVMs;

namespace TestProject.Controllers
{
    public class AccountController : Controller
    {
        private readonly ILogger<AccountController> _logger;
        private readonly FastScoreContext context;
        private readonly IUnitOfWork unitOfWork;
        private readonly Microsoft.AspNetCore.Identity.UserManager<User> userManager;
        private readonly SignInManager<User> signInManager;
        private readonly RoleManager<Role> roleManager;

        public AccountController(ILogger<AccountController> logger, FastScoreContext context, IUnitOfWork unitOfWork,
            UserManager<User> userManager, SignInManager<User> signInManager, RoleManager<Role> roleManager)
        {
            _logger = logger;
            this.context = context;
            this.unitOfWork = unitOfWork;
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.roleManager = roleManager;
        }

        [Authorize]
        public async Task<IActionResult> Logout()
        {
            await signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login(UserLoginViewModel userLoginVM, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var result = await signInManager.PasswordSignInAsync(userLoginVM.UserName, userLoginVM.Password, userLoginVM.RememberMe, false);

                if (result.Succeeded)
                {
                    if (returnUrl != null)
                    {
                        return Redirect(returnUrl);
                    }

                    return RedirectToAction("Index", "Home");
                }

                ModelState.AddModelError("", "Invalid login attempt");
            }

            return View();
        }

        [HttpGet]
        [Authorize(Roles = UserRoleConsts.ADMIN)]
        public async Task<IActionResult> GetAll()
        {
            var users = await userManager.Users.ToListAsync();
            return View(users);
        }

        [HttpGet]
        [Authorize(Roles = UserRoleConsts.ADMIN)]
        public async Task<IActionResult> SearchBy(UserFilteringCriterias userFilteringCriterias)
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public async Task AddMatchToFavorites(int matchId)
        {
            var user = await userManager.GetUserAsync(User);

            if (user == null)
            {
                throw new ArgumentNullException();
            }

            await this.unitOfWork.AccountService.AddMatchToFavoritesAsync(matchId, user.Id);
        }

        [HttpPost]
        [Authorize]
        public async Task RemoveMatchFromFavorites(int matchId)
        {
            var user = await userManager.GetUserAsync(User);

            if (user == null)
            {
                throw new ArgumentNullException();
            }

            await this.unitOfWork.AccountService.RemoveMatchFromFavoritesAsync(matchId, user.Id);
        }

        [HttpPost]
        public async Task RemoveLeagueFromFavorites(int leagueId)
        {
            var user = await userManager.GetUserAsync(User);

            if (user == null)
            {
                throw new ArgumentNullException();
            }

            await this.unitOfWork.AccountService.RemoveLeagueFromFavoritesAsync(leagueId, user.Id);
        }

        [HttpPost]
        public async Task<IActionResult> AddLeagueToFavorites(int leagueId)
        {
            var user = await userManager.GetUserAsync(User);

            if (user == null)
            {
                throw new ArgumentNullException();
            }

            var league = await this.unitOfWork.AccountService.AddLeagueToFavoritesAsync(leagueId, user.Id);
            return Json(league);
        }

        [HttpPost]
        public async Task AddAllLeagueMatchesToFavorites(int leagueId, DateTime date)
        {
            var user = await userManager.GetUserAsync(User);

            if(user == null)
            {
                throw new ArgumentNullException();
            }

            await unitOfWork.AccountService.AddAllLeagueMatchesToFavorites(leagueId, user.Id, date);
        }

        [HttpPost]
        public async Task RemoveAllLeagueMatchesFromFavorites(int leagueId, DateTime date)
        {
            var user = await userManager.GetUserAsync(User);

            if (user == null)
            {
                throw new ArgumentNullException();
            }

            await unitOfWork.AccountService.RemoveAllLeagueMatchesFromFavorites(leagueId, user.Id, date);
        }

        [HttpGet]
        [Authorize(Roles = UserRoleConsts.ADMIN)]
        public async Task<IActionResult> UpdateUsersRole(string roleId)
        {
            var usersInRole = new List<EditUsersRoleViewModel>();
            var role = await roleManager.FindByIdAsync(roleId);
            var users = await userManager.Users.ToListAsync();

            foreach (var user in users)
            {
                var editUserRoleViewModel = new EditUsersRoleViewModel()
                {
                    UserId = user.Id,
                    UserName = user.UserName
                };

                if (await userManager.IsInRoleAsync(user, role.Name))
                {
                    editUserRoleViewModel.IsSelected = true;
                }

                usersInRole.Add(editUserRoleViewModel);
            }

            return View(usersInRole);
        }

        [HttpPost]
        [Authorize(Roles = UserRoleConsts.ADMIN)]
        public async Task<IActionResult> UpdateUsersRole(IEnumerable<EditUsersRoleViewModel> usersInRole, string roleId)
        {
            if (usersInRole == null)
            {
                throw new ArgumentNullException();
            }

            var role = await roleManager.FindByIdAsync(roleId);

            if (role == null)
            {
                throw new ArgumentNullException();
            }

            foreach (var vmUser in usersInRole)
            {
                var user = await this.context.Users.FirstOrDefaultAsync(u => u.Id == vmUser.UserId);

                var isInRole = await userManager.IsInRoleAsync(user, role.Name);

                if (!vmUser.IsSelected && isInRole)
                {
                    await userManager.RemoveFromRoleAsync(user, role.Name);
                }
                else if (vmUser.IsSelected && !isInRole)
                {
                    await userManager.AddToRoleAsync(user, role.Name);
                }
            }

            return RedirectToAction("Edit", "Roles", new { roleId = role.Id });
        }


        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Register(UserRegisterViewModel userRegisterVM)
        {
            if (ModelState.IsValid)
            {
                var user = InputMapper.Map(userRegisterVM);
                var result = await userManager.CreateAsync(user, userRegisterVM.Password);

                if (result.Succeeded)
                {
                    var dbUser = await userManager.FindByNameAsync(userRegisterVM.UserName);
                    await this.context.Entry(dbUser).Collection(u => u.Leagues).LoadAsync();

                    var favoriteLeaguesIds = new List<int>()
                    {
                        LeagueIdsConsts.PREMIER_LEAGUE_ID, LeagueIdsConsts.LA_LIGA_ID, LeagueIdsConsts.BUNDESLIGA_ID
                    };

                    var favoriteLeagues = await this.context.Leagues.Where(l => favoriteLeaguesIds.Contains(l.Id)).ToListAsync();
                    dbUser.Leagues.AddRange(favoriteLeagues);
                    await context.SaveChangesAsync();
                    await signInManager.SignInAsync(user, isPersistent: false);
                    return RedirectToAction("Index", "Home");
                }

                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
            }

            return View();
        }
    }
}
