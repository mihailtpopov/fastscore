﻿
(() => {
    var timeout;
    var popupContainer = $('.outerContainer');
    var messageElement = $('.content');

    ShowFriendlyCoordinationMessage(popupContainer, messageElement);
    ManageUserFavoritesMatches();
    SetOnLoadJS();

    function ShowElementNextToCursor(popupContainer, messageElement, message, event) {

        clearTimeout(timeout);

        timeout = setTimeout(() => {
            popupContainer.css({ 'display': 'table', 'left': (event.clientX + 5) + 'px', 'top': (event.clientY + 14) + 'px' });
            messageElement.text(message);
        }, 1000);

        popupContainer.css('display', 'none');
    }
    function ShowFriendlyCoordinationMessage(popupContainer, messageElement) {
        $('.match-div').mousemove((event) => {
            let message = 'Click for match detail!';
            ShowElementNextToCursor(popupContainer, messageElement, message, event);
        });

        $('.match-star').mousemove((event) => {
            let message = 'Click to add to Favorites!';
            ShowElementNextToCursor(popupContainer, messageElement, message, event);
        });

        $('.special-container-match').on('mouseleave', () => {
            clearTimeout(timeout)
        });
    }

    function ManageUserFavoritesMatches() {
        $('.match-star').click(function () {
            let star = $('.match-star-img');
            let matchId = $(this).attr('id');

            if (star.attr('src') == '/imges/empty_star.png') {
                $.ajax({
                    method: "POST",
                    url: "/Account/AddMatchToFavorites",
                    data: { matchId: matchId },
                    success: () => {
                        star.attr('src', '/imges/full_star.png');
                        $('.outer-match-added-div').css('display', 'table');
                        $('.inner-match-added-div #content').text('Added to Favorites.');
                        let favoriteMatchesCount = $('#favoritesDot').text();
                        $('#favoritesDot').text(+favoriteMatchesCount + 1);
                    },
                    error: () => {
                        window.location.href = "/Account/Login";
                    }
                });
            }
            else {
                $.ajax({
                    method: "POST",
                    url: "/Account/RemoveMatchFromFavorites",
                    data: { matchId: matchId },
                    success: () => {
                        star.attr('src', '/imges/empty_star.png');
                        $('.outer-match-added-div').css('display', 'table');
                        $('.inner-match-added-div #content').text('Removed from Favorites.');
                        let favoriteMatchesCount = $('#favoritesDot').text();
                        $('#favoritesDot').text(+favoriteMatchesCount - 1);
                    }
                });
            }
        });
    }

    function SetOnLoadJS() {

        $('.match-info').css('text-align', 'center');

        if ($('.live-bet[matchStatusId=2]').length > 0) {

            $('.live-bet[matchStatusId=2]').css({ 'background-color': 'red', 'color': 'white' });

            let currMinuteWidthArr = $('.curr-minute').css('width').split('px');
            let tickSignWidthArr = $('.tick-sign').css('width').split('px');
            let minWidth = +currMinuteWidthArr[0] + (+tickSignWidthArr[0]);
            $('.match-info-curr-minute').css('min-width', minWidth + 'px');

            setInterval(() => {
                let color = $('.tick-sign').css('color');

                $('.tick-sign').toggle();
                $('.live-bet[matchStatusId=2]').toggle();
            }, 1000);


            $('.curr-minute').each(function (index, element) {

                let halfTimeInterval = setInterval(function () {

                    let currMin = +$(element).text();
                    $(element).text(currMin + 1);

                    if ($(element).text() == "46") {

                        $(element).text('Half-time');
                        $(`.tick-sign[matchId="${$(this).attr('matchId')}"`).css('display', 'none');
                        SetMatchInfoMaxWidth();

                        clearInterval(halfTimeInterval);

                        let halfTimeBreak = setTimeout(() => {
                            let currMin = 45;

                            let secondHalfTimeInterval = setInterval(() => {

                                clearTimeout(halfTimeBreak);

                                currMin += 1;
                                $(element).text(currMin);

                                if ($(element).text() == '91') {

                                    clearInterval(secondHalfTimeInterval);

                                    $(element).text('Finished');
                                    $(element).css('color', 'black');

                                    let matchCurrResult = $(`.match-current-result[matchId="${$(element).attr('matchId')}"`);

                                    let homeTeamGoals = +matchCurrResult.children().first().text();
                                    let awayTeamGoals = +matchCurrResult.children().last().text();

                                    if (homeTeamGoals > awayTeamGoals) {
                                        let matchId = $(element).attr('matchId');
                                        let homeTeamName = $(`.home-team-name[matchId="${matchId}"]`);
                                        homeTeamName.css('font-weight', 'bold');
                                    }
                                    else if (awayTeamGoals > homeTeamGoals) {
                                        $(`.away-team-name[matchId="${$(element).attr('matchId')}"]`).css('font-weight', 'bold');
                                    }

                                    matchCurrResult.css({ 'color': 'black', 'font-weight': 'bold' });
                                }

                            }, 60000);
                        }, 900000);

                    }

                }, 60000)
            });
        }

        let finishedMatchesStatus = $(`#match-finished.match-info`);
        if (finishedMatchesStatus.length > 0) {

            finishedMatchesStatus.each(function () {
                let matchResult = $(`.match-final-result[matchId="${$(this).attr('matchId')}"`);

                matchResult.css('font-weight', 'bold');

                let homeTeamScore = +matchResult.children().first().text();
                let awayTeamScore = +matchResult.children().last().text();

                if (homeTeamScore > awayTeamScore) {
                    $(`.home-team-name[matchId="${$(this).attr('matchId')}"`).css('font-weight', 'bold');
                }
                else if (awayTeamScore > homeTeamScore) {
                    $(`.away-team-name[matchId="${$(this).attr('matchId')}"`).css('font-weight', 'bold');
                }
            });
        }

        let scheduledMatchesStartingTimes = $('#match-starting-time.match-info');

        if (scheduledMatchesStartingTimes.length > 0) {
            setInterval(function () {
                scheduledMatchesStartingTimes.each(function () {
                    let timeAsArr = $(this).text().split(':');
                    let startingTime = { hour: +timeAsArr[0], minute: +timeAsArr[1] };
                    let today = new Date();

                    if (startingTime.hour == today.getHours() && startingTime.minute == today.getMinutes()) {

                        $(this).replaceWith(`<div class="match-info">
                                                <span class= "match-info-curr-minute" matchId="${$(this).attr('matchId')}">
                                                    <span class="curr-minute" matchId="${$(this).attr('matchId')}">1</span><span class="tick-sign">'</span>
                                                </span>
                                             </div>`);

                        $(`.match-info-curr-minute[matchId="${$(this).attr('matchId')}"`).children('.curr-minute').css('color', 'red');
                        $(`.match-info-curr-minute[matchId="${$(this).attr('matchId')}"`).children('.tick-sign').toggle();
                        $(`.match-info-curr-minute[matchId="${$(this).attr('matchId')}"`).css('text-align', 'center');

                        SetMatchInfoMaxWidth();
                    }
                })
            }, 60000)
        }

        $('curr-minute').css()

        SetMatchInfoMaxWidth();
    }

    function SetMatchInfoMaxWidth() {
        let widthsArr = $('.match-info').map(function (index, element) {
            let width = +$(element).css('width').split('px')[0];
            return [width];
        }).toArray();

        let maxWidth = Math.max(...widthsArr);
        $('.match-info').css('width', `${maxWidth}`);
    }
})();
