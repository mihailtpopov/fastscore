﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Models
{
    public class Card
    {
        public int Id { get; set; }

        public int CardTypeId { get; set; }

        public CardType CardType {get;set;}

        public int ReceiverId { get; set; }

        public Player Receiver { get; set; }
    }
}
