﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Data.Models
{
    public class Country
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public string FlagPhotoUrl { get; set; }

        public IList<League> Leagues { get; set; }

        public IList<Sport> Sports { get; set; }
    }
}
