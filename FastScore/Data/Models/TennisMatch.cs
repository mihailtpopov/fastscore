﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Data.Models
{
    class TennisMatch
    {
        [Key]
        public int Id { get; set; }

        public int LeagueId { get; set; }
        public League League { get; set; }

        public int SeasonYear { get; set; }

        public DateTime StartingTime { get; set; }

        public string RefereeName { get; set; }

        public bool HasPreview { get; set; }

        public int PlayerOneId { get; set; }
        public Player PlayerOne { get; set; }

        public int PlayerTwoId { get; set; }
        public Team PlayerTwo { get; set; }

        public int WinnerId { get; set; }
        public Player Winner { get; set; }

        public int LosserId { get; set; }
        public Player Losser { get; set; }

        public int MatchStatusId { get; set; }
        public MatchStatus MatchStatus { get; set; }

        public Odds Odds { get; set; }

        public IList<User> Favoritiees { get; set; }
    }
}
