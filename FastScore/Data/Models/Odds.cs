﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Data.Models
{
    public class Odds
    {
        [Key]
        public int Id { get; set; }

        public int MatchId { get; set; }

        public Match Match { get; set; }

        public double HomeWin { get; set; }

        public double AwayWin { get; set; }

        public double Draw { get; set; }

        public double Over2o5 { get; set; }

        public double Under2o5 { get; set; }
    }
}
