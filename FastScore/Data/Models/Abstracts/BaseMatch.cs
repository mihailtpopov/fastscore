﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Data.Models.Abstracts
{
    public abstract class BaseMatch
    {
        [Key]
        public int Id { get; set; }

        public FirstHalf FirstHalf { get; set; }

        public SecondHalf SecondHalf { get; set; }

        public int LeagueId { get; set; }
        public League League { get; set; }

        public int SeasonYear { get; set; }

        public DateTime StartingTime { get; set; }

        public string RefereeName { get; set; }

        public bool HasPreview { get; set; }

        [ForeignKey("HomeTeam")]
        public int HomeTeamId { get; set; }
        public Team HomeTeam { get; set; }

        public int AwayTeamId { get; set; }
        public Team AwayTeam { get; set; }

        public int? WinnerId { get; set; }
        public Team Winner { get; set; }

        public int? LosserId { get; set; }
        public Team Losser { get; set; }

        public int MatchStatusId { get; set; }
        public MatchStatus MatchStatus { get; set; }

        public Odds Odds { get; set; }

        public IList<User> Favoritiees { get; set; }
    }
}
