﻿using AutoMapper;
using Data.Models;
using Data.Models.Abstracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services
{
    class MatchProfile : Profile
    {
        public MatchProfile()
        {
            CreateMap<IEnumerable<BaseMatch>, IEnumerable<MatchArchive>>();
            CreateMap<IEnumerable<Match>, IEnumerable<MatchArchive>>();
        }
    }
}
