﻿using Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Mapping
{
    public static class BaseEntityMapper
    {
        public static IEnumerable<MatchArchive> Map(IEnumerable<Match> matches)
        {
            List<MatchArchive> matchArchives = new List<MatchArchive>();

            foreach (var match in matches)
            {
                matchArchives.Add(new MatchArchive()
                {
                    AwayTeamId = match.AwayTeamId,
                    HomeTeamId = match.HomeTeamId,
                    HasPreview = match.HasPreview,
                    LeagueId = match.LeagueId,
                    LosserId = match.LosserId,
                    WinnerId = match.WinnerId,
                    MatchStatusId = match.MatchStatusId,
                    RefereeName = match.RefereeName,
                    SeasonYear = match.SeasonYear,
                    StartingTime = match.StartingTime
                });
            }

            return matchArchives;
        }
    }
}
