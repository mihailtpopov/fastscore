﻿using AutoMapper;
using Data;
using Services.Contracts;
using Services.Services;
using System;
using System.Threading.Tasks;

namespace Services
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly FastScoreContext context;
        private readonly IMapper mapper;
        private IAccountService accountService;
        private ISportsService sportsService;
        private IMatchesService matchesService;
        private IFirstHalfsService firstHalfsService;
        private ISecondHalfsService secondHalfsService;

        public UnitOfWork(FastScoreContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public IFirstHalfsService FirstHalfsService
        {
            get
            {
                if(this.firstHalfsService == null)
                {
                    this.firstHalfsService = new FirstHalfsService(context, this);
                }

                return this.firstHalfsService;
            }
        }
        
        public ISecondHalfsService SecondHalfsService
        {
            get
            {
                if(this.secondHalfsService == null)
                {
                    this.secondHalfsService = new SecondHalfsService(context, this);
                }

                return this.secondHalfsService;
            }
        }

        public IAccountService AccountService
        {
            get
            {
                if (this.accountService == null)
                {
                    this.accountService = new AccountService(this.context);
                }

                return this.accountService;
            }
        }

        public ISportsService SportsService
        {
            get
            {
                if (this.sportsService == null)
                {
                    this.sportsService = new SportsService(this.context);
                }

                return this.sportsService;
            }
        }

        public IMatchesService MatchesService
        {
            get
            {
                if (this.matchesService == null)
                {
                    this.matchesService = new MatchesService(this.context, this.mapper);
                }

                return this.matchesService;
            }
        }

        public async Task SaveChangesAsync()
        {
            try
            {
                await this.context.SaveChangesAsync();
            }
            catch(Exception e)
            {
                throw new InvalidOperationException(e.InnerException.Message);
            }
        }
    }
}
