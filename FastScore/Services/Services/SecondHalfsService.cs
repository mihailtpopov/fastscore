﻿using Data;
using Data.Models;
using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Services
{
    public class SecondHalfsService : ISecondHalfsService
    {
        private readonly FastScoreContext context;
        private readonly UnitOfWork unitOfWork;

        public SecondHalfsService(FastScoreContext context, UnitOfWork unitOfWork)
        {
            this.context = context;
            this.unitOfWork = unitOfWork;
        }

        public async Task Create(int matchId)
        {
            var match = unitOfWork.MatchesService.GetByIdAsync(matchId);

            if (match == null)
            {
                throw new ArgumentNullException();
            }

            var secondHalf = new SecondHalf() { MatchId = matchId, CurrMinute = 1 };

            await this.context.SecondHalfs.AddAsync(secondHalf);
            await this.context.SaveChangesAsync();
        }
    }
}
