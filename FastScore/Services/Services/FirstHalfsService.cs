﻿using Data;
using Data.Models;
using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Services
{
    public class FirstHalfsService : IFirstHalfsService
    {
        private readonly FastScoreContext context;
        private readonly UnitOfWork unitOfWork;

        public FirstHalfsService(FastScoreContext context, UnitOfWork unitOfWork)
        {
            this.context = context;
            this.unitOfWork = unitOfWork;
        }

        public async Task Create(int matchId)
        {
            var match = unitOfWork.MatchesService.GetByIdAsync(matchId);

            if (match == null)
            {
                throw new ArgumentNullException();
            }

            var firstHalf = new FirstHalf() { MatchId = matchId, CurrMinute = 1 };

            await this.context.FirstHalfs.AddAsync(firstHalf);
            await this.context.SaveChangesAsync();
        }
    }
}
