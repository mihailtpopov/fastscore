﻿using AutoMapper;
using Consts;
using Data;
using Data.Models;
using Data.Models.Abstracts;
using Microsoft.EntityFrameworkCore;
using Services.Contracts;
using Services.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Services.Services
{
    public class MatchesService : IMatchesService
    {
        private readonly FastScoreContext context;
        private readonly IMapper mapper;

        public MatchesService(FastScoreContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<Match> GetByIdAsync(int matchId)
        {
            var match = await this.context.Matches.FirstOrDefaultAsync(m => m.Id == matchId);

            if (match == null)
            {
                throw new ArgumentNullException();
            }

            return match;
        }

        public async Task<IDictionary<League, IEnumerable<Match>>> GetAll(int sportId, DateTime date)
        {
            var leaguesMatchesWithIncludes = this.GetLeaguesWithIncludesHalfs();
            var leaguesMatches = await this.GetLeaguesMatchesByDateSportId(leaguesMatchesWithIncludes, sportId, date);

            return leaguesMatches;
        }

        public async Task<IDictionary<League, IEnumerable<Match>>> GetAllWithOdds(int sportId, DateTime date)
        {
            if ((await this.context.Sports.FindAsync(sportId)) == null)
            {
                throw new ArgumentNullException();
            }

            var leaguesMatches = await this.GetLeaguesMatchesByDateSportId(this.GetLeaguesWithMatchesOdds(), sportId, date);

            return leaguesMatches;
        }

        public async Task<IDictionary<League, IEnumerable<Match>>> GetByDate(int sportId, DateTime date)
        {
            if ((await this.context.Sports.FindAsync(sportId)) == null)
            {
                throw new ArgumentNullException();
            }

            IDictionary<League, IEnumerable<Match>> dateMatches = null;

            if (date > DateTime.Today)
            {
                var leaguesWithIncludes = this.GetLeaguesWithIncludes();
                dateMatches = await this.GetLeaguesMatchesByDateSportId(leaguesWithIncludes, sportId, date);
            }
            else
            {
                var leaguesWithIncludes = this.GetLeaguesWithIncludesHalfs();
                dateMatches = await this.GetLeaguesMatchesByDateSportId(leaguesWithIncludes, sportId, date);
            }

            return dateMatches;
        }

        public async Task AddOutOfDateMatchesToArchive()
        {
            var outOfDateMatches = await this.GetOutOfDateMatches();
            var outOfDateMatchesToArchive = BaseEntityMapper.Map(outOfDateMatches);

            this.context.Matches.RemoveRange();
            await this.context.MatchesArchive.AddRangeAsync(outOfDateMatchesToArchive);

            await context.SaveChangesAsync();
        }

        public int GetTodayMatchesCount(int sportId)
        {
            var todayMatchesCount = this.context.Matches
                .Include(m => m.League)
                .Where(m => m.League.SportId == sportId && m.StartingTime.Date == DateTime.Today)
                .Count();

            return todayMatchesCount;
        }

        public async Task<IDictionary<League, IEnumerable<Match>>> GetByMatchStatusId(int sportId, int matchStatusId, DateTime date)
        {
            if ((await this.context.Sports.FindAsync(sportId)) == null)
            {
                throw new ArgumentNullException();
            }

            if ((await this.context.MatchStatuses.FindAsync(matchStatusId)) == null)
            {
                throw new ArgumentNullException();
            }

            IDictionary<League, IEnumerable<Match>> dateMatchStatusMatches = null;

            switch (matchStatusId)
            {
                case MatchStatusIdsConsts.FINISHED:
                case MatchStatusIdsConsts.IN_PLAY:
                    dateMatchStatusMatches = await this.GetLeaguesMatchesByDateSportIdMatchStatusId(this.GetLeaguesWithIncludesHalfs(), sportId, date, matchStatusId);
                    break;

                case MatchStatusIdsConsts.SCHEDULED:
                    dateMatchStatusMatches = await this.GetLeaguesMatchesByDateSportIdMatchStatusId(this.GetLeaguesWithIncludes(), sportId, date, matchStatusId);
                    break;
            }

            return dateMatchStatusMatches;
        }

        private async Task<IEnumerable<Match>> GetOutOfDateMatches()
        {
            var date = DateTime.Today.AddDays(-7);
            var outOfDateMatches = await context.Matches.Where(m => m.StartingTime < date).ToListAsync();

            return outOfDateMatches;
        }

        private IQueryable<League> GetLeaguesWithIncludes()
        {
            var leagues = this.context.Leagues
           .Include(l => l.Matches)
               .ThenInclude(m => m.HomeTeam)
           .Include(l => l.Matches)
               .ThenInclude(m => m.AwayTeam)
           .Include(l => l.Country);

            return leagues;
        }

        private IQueryable<League> GetLeaguesWithIncludesHalfs()
        {
            var leagues = this.context.Leagues
           .Include(l => l.Matches)
               .ThenInclude(m => m.HomeTeam)
           .Include(l => l.Matches)
               .ThenInclude(m => m.AwayTeam)
           .Include(l => l.Matches)
               .ThenInclude(m => m.FirstHalf)
           .Include(l => l.Matches)
               .ThenInclude(m => m.SecondHalf)
           .Include(l => l.Matches)
               .ThenInclude(m => m.FirstHalf.HomeGoals)
           .Include(l => l.Matches)
                .ThenInclude(m => m.FirstHalf.AwayGoals)
           .Include(l => l.Matches)
                .ThenInclude(m => m.SecondHalf.HomeGoals)
           .Include(l => l.Matches)
                .ThenInclude(m => m.SecondHalf.AwayGoals)
           .Include(l => l.Country);

            return leagues;
        }

        private IQueryable<League> GetLeaguesWithMatchesOdds()
        {
            var leagues = this.GetLeaguesWithIncludesHalfs()
                .Include(l => l.Matches)
                    .ThenInclude(m => m.Odds);

            return leagues;
        }

        private async Task<Dictionary<League, IEnumerable<Match>>> GetLeaguesMatchesByDateSportId(IQueryable<League> leagues, int sportId, DateTime date)
        {
            var leaguesMatches = await leagues.Where(l => l.SportId == sportId && l.Matches.Any(m => m.StartingTime.Date == date))
                   .Select(l => new KeyValuePair<League, IEnumerable<Match>>(l, l.Matches.Where(m => m.StartingTime.Date == date)))
                   .ToDictionaryAsync(kvp => kvp.Key, kvp => kvp.Value);

            return leaguesMatches;
        }

        private async Task<Dictionary<League, IEnumerable<Match>>> GetLeaguesMatchesByDateSportIdMatchStatusId(IQueryable<League> leaguesWithIncludes, int sportId, DateTime date, int matchStatusId)
        {
            var leaguesMatches = await leaguesWithIncludes.Where(l => l.SportId == sportId && l.Matches.Any(m => m.StartingTime.Date == date && m.MatchStatusId == matchStatusId))
                       .Select(l => new KeyValuePair<League, IEnumerable<Match>>(l, l.Matches.Where(m => m.StartingTime.Date == date && m.MatchStatusId == matchStatusId)))
                       .ToDictionaryAsync(p => p.Key, p => p.Value);

            return leaguesMatches;
        }
    }
}
