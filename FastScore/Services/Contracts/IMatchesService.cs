﻿using Data;
using Data.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Contracts
{
    public interface IMatchesService
    {
        Task AddOutOfDateMatchesToArchive();
        Task<IDictionary<League, IEnumerable<Match>>> GetAll(int sportId, DateTime date);
        Task<IDictionary<League, IEnumerable<Match>>> GetAllWithOdds(int sportId, DateTime date);
        Task<IDictionary<League, IEnumerable<Match>>> GetByDate(int sportId, DateTime date);
        Task<Match> GetByIdAsync(int matchId);
        Task<IDictionary<League, IEnumerable<Match>>> GetByMatchStatusId(int sportId, int matchStatusId, DateTime date);
        int GetTodayMatchesCount(int sportId);
    }
}