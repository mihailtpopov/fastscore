﻿using System.Threading.Tasks;

namespace Services.Contracts
{
    public interface ISecondHalfsService
    {
        Task Create(int matchId);
    }
}