﻿using Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Contracts
{
    public interface ISportsService
    {
        Task<IEnumerable<Sport>> GetAll();

        Task<Sport> GetByIdAsync(int sportId);

    }
}