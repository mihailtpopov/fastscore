﻿using Services.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts
{
    public interface IUnitOfWork
    {

        public IAccountService AccountService { get; }

        public ISportsService SportsService { get; }

        public IMatchesService MatchesService { get; }

        public IFirstHalfsService FirstHalfsService { get; }

        public ISecondHalfsService SecondHalfsService { get; }

        public Task SaveChangesAsync();
    }
}
