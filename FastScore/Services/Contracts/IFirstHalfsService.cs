﻿using System.Threading.Tasks;

namespace Services.Contracts
{
    public interface IFirstHalfsService
    {
        Task Create(int matchId);
    }
}