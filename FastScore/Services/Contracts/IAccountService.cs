﻿using Data;
using Data.Models;
using Data.Models.FilteringModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Contracts
{
    public interface IAccountService
    {
        Task AddAllLeagueMatchesToFavorites(int leagueId, string userId, DateTime date);

        Task<League> AddLeagueToFavoritesAsync(int leagueId, string userId);

        Task AddMatchToFavoritesAsync(int matchId, string userId);

        Task<User> GetByIdAsync(string id);

        Task RemoveAllLeagueMatchesFromFavorites(int leagueId, string userId, DateTime date);

        Task RemoveLeagueFromFavoritesAsync(int leagueId, string userId);

        Task RemoveMatchFromFavoritesAsync(int matchId, string userId);

        Task<IEnumerable<User>> SearchByAsync(UserFilteringCriterias userFilteringCriterias);
    }
}