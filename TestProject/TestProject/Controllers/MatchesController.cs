﻿using Data;
using Data.Models;
using Microsoft.AspNetCore.Mvc;
using Services;
using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestProject.Mapping;

namespace TestProject.Controllers
{
    public class MatchesController : Controller
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly FastScoreContext context;

        public MatchesController(IUnitOfWork unitOfWork, FastScoreContext context)
        {
            this.unitOfWork = unitOfWork;
            this.context = context;
        }

        public async Task<IActionResult> GetByDate(int sportId, DateTime date)
        {
            var matchesByDate = await unitOfWork.MatchesService.GetByDate(sportId, date);

            if (!matchesByDate.Any())
            {
                var sport = await this.unitOfWork.MatchesService.GetByIdAsync(sportId);

                return PartialView("_NoMatchFound", sport.PhotoUrl);
            }

            var matchesByDateVM = ViewModelMapper.Map(matchesByDate);

            return PartialView("_GetMatches", matchesByDateVM);
        }

        public async Task<IActionResult> GetByMatchStatusId(int sportId, int matchStatusId, DateTime date)
        {
            var matchesByMatchStatusId = await unitOfWork.MatchesService.GetByMatchStatusId(sportId, matchStatusId, date);

            if (!matchesByMatchStatusId.Any())
            {
                var sport = await this.unitOfWork.MatchesService.GetByIdAsync(sportId);

                return PartialView("_NoMatchFound", sport.PhotoUrl);
            }

            return View("GetByDate", model: matchesByMatchStatusId);
        }
    }
}
