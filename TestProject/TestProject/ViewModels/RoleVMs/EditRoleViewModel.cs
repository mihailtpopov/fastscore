﻿using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestProject.ViewModels.RoleVMs
{
    public class RoleInfoViewModel
    {
        public string RoleId { get; set; }

        public string RoleName { get; set; }

        public IList<User> UsersInRole { get; set; } = new List<User>();
    }
}
