﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestProject.ViewModels
{
    public class MatchViewModel
    {
        public int MatchId { get; set; }

        public string StartingTime { get; set; }

        public int MatchStatusId { get; set; }

        public int? FirstHalfCurrMinute { get; set; }

        public int? SecondHalfCurrMinute { get; set; }

        public string HomeTeamName { get; set; }

        public string HomeTeamFlagPhotoUrl { get; set; }

        public string AwayTeamName { get; set; }

        public string AwayTeamFlagPhotoUrl { get; set; }

        public bool HasPreview { get; set; }

        public int FirstHalfHomeGoalsCount { get; set; }

        public int FirstHalfAwayGoalsCount { get; set; }

        public int SecondHalfHomeGoalsCount { get; set; }

        public int SecondHalfAwayGoalsCount { get; set; }
    }
}
