#pragma checksum "C:\Users\MichaelP\source\repos\fastscore\TestProject\TestProject\Views\Account\Privacy.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "d8ddb6bffa5a9b264bf8f89038bf03c234083fd3"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Account_Privacy), @"mvc.1.0.view", @"/Views/Account/Privacy.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\MichaelP\source\repos\fastscore\TestProject\TestProject\Views\_ViewImports.cshtml"
using TestProject;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\MichaelP\source\repos\fastscore\TestProject\TestProject\Views\_ViewImports.cshtml"
using TestProject.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\MichaelP\source\repos\fastscore\TestProject\TestProject\Views\_ViewImports.cshtml"
using TestProject.ViewModels;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\MichaelP\source\repos\fastscore\TestProject\TestProject\Views\_ViewImports.cshtml"
using System.Security.Claims;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\MichaelP\source\repos\fastscore\TestProject\TestProject\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\MichaelP\source\repos\fastscore\TestProject\TestProject\Views\_ViewImports.cshtml"
using Data.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\MichaelP\source\repos\fastscore\TestProject\TestProject\Views\_ViewImports.cshtml"
using TestProject.ViewModels.RoleVMs;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\MichaelP\source\repos\fastscore\TestProject\TestProject\Views\_ViewImports.cshtml"
using TestProject.ViewModels.UserVMs;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\MichaelP\source\repos\fastscore\TestProject\TestProject\Views\_ViewImports.cshtml"
using Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\MichaelP\source\repos\fastscore\TestProject\TestProject\Views\_ViewImports.cshtml"
using Data;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Users\MichaelP\source\repos\fastscore\TestProject\TestProject\Views\_ViewImports.cshtml"
using Microsoft.EntityFrameworkCore;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\Users\MichaelP\source\repos\fastscore\TestProject\TestProject\Views\_ViewImports.cshtml"
using Consts;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\Users\MichaelP\source\repos\fastscore\TestProject\TestProject\Views\_ViewImports.cshtml"
using Services.Contracts;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d8ddb6bffa5a9b264bf8f89038bf03c234083fd3", @"/Views/Account/Privacy.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"4baefd495e0f2e4db3184bed0277fd72cd726cd0", @"/Views/_ViewImports.cshtml")]
    public class Views_Account_Privacy : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "C:\Users\MichaelP\source\repos\fastscore\TestProject\TestProject\Views\Account\Privacy.cshtml"
  
    ViewData["Title"] = "Privacy Policy";

#line default
#line hidden
#nullable disable
            WriteLiteral("<h1>");
#nullable restore
#line 4 "C:\Users\MichaelP\source\repos\fastscore\TestProject\TestProject\Views\Account\Privacy.cshtml"
Write(ViewData["Title"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h1>\r\n\r\n<p>Use this page to detail your site\'s privacy policy.</p>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
