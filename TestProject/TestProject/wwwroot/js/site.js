﻿(() => {
    SetOnLoadCSS();
    SetOnLoadJS();
    BindEvents();
})();

function BindEvents() {
    ManageMatchesByDate();
    ManageUserFavorites();
    ManageMatchesByStatus();
    ManageMatchesBySport();
}

function SetOnLoadCSS() {
    $('#currSport').css('background-color', 'white');
    $('#currSport').children('.text').css('color', 'black');
    $('#all-games').css({ 'background-color': 'darkgreen', 'color': 'white' });

    let widthsArr = $('.match-info').map(function (index, element) {
        let width = +$(element).css('width').split('px')[0];
        return [width];
    }).toArray();

    let maxWidth = Math.max(...widthsArr);
    $('.match-info').css('width', `${maxWidth}`);
}

function SetOnLoadJS() {
    let date = $('#calendar-date').text();
    $('#custom-dropdown-today-a').data('todayDate', date);

    let oneBeforeLatestDate = $('.custom-dropdown-item').eq(-2).text().trim();
    let latestDate = $('.custom-dropdown-item').eq(-1).text().trim();
    $('#calendar-date').data('oneBeforeLatestDate', oneBeforeLatestDate);
    $('#calendar-date').data('latestDate', latestDate);

    let oneBeforeEarliestDate = $('.custom-dropdown-item').eq(1).text().trim();
    let earliestDate = $('.custom-dropdown-item').eq(0).text().trim();
    $('#calendar-date').data('oneBeforeEarliestDate', oneBeforeEarliestDate);
    $('#calendar-date').data('earliestDate', earliestDate);
}

function ConvertStringToDate(stringDate) {
    let splittedDate = stringDate.split('/').join(' ').split(' ');
    let year = new Date().getFullYear();

    return new Date(`${year}/${splittedDate[1]}/${splittedDate[0]}`);
}

function ChangeManageDateButtonsOnClick(element) {

    let filtered = $('.menu').filter(function (index, el) {
        return $(el).css('background-color') === "rgb(0, 100, 0)";
    })

    if (filtered.length > 0) {
        filtered.each(function () {
            $(this).css('color', 'black');
            $(this).css('background-color', 'lightgray');
        })
    }

    $(element).children().css('background-color', 'darkgreen');
    $(element).children().css('color', 'white');
}

function GetFormattedDate(dateToFormat) {
    let day = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(dateToFormat);
    let month = new Intl.DateTimeFormat('en', { month: '2-digit' }).format(dateToFormat);
    let weekday = new Intl.DateTimeFormat('en', { weekday: 'short' }).format(dateToFormat);

    return { day: day, month: month, weekday: weekday };
}

function ManageMatchesByDate() {

    $('#calendar-date').on('changeManageDateButtons', function () {
        if ($(this).text().trim() == $(this).data('earliestDate')) {
            $('#left-arrow').css('display', 'none');
        }
        else if ($(this).text().trim() == $(this).data('latestDate')) {
            $('#right-arrow').css('display', 'none');
        } else {
            let notDisplayedArrow = $('.arrow').filter((index, element) => { return $(element).css('display', 'none') });

            if (notDisplayedArrow.length > 0) {
                notDisplayedArrow.css('display', 'flex');
            }
        }
    });

    $('#custom-dropdown-today-a').click(function () {

        let todayDate = $(this).data('todayDate');
        let date = ConvertStringToDate(todayDate);


        if (todayDate != $('#calendar-date').text()) {
            $.ajax({
                url: '/Matches/GetByDate',
                method: 'GET',
                data: { sportId: 1, date: date.toLocaleDateString("en-US") },
                success: (resultHtml) => {
                    $('#date-dropdown').css('display', 'none');
                    $('#calendar-date').text(todayDate);
                    let sda = $('#partial-body-content');
                    $('#partial-body-content').html(resultHtml);
                    $('#calendar-date').trigger('changeManageDateButtons');
                }
            })
        } else {
            $('.custom-dropdown').css('display', 'none');
        }
    })

    $('#not-today-date.custom-dropdown-item').click(function (event) {
        let dateAsString = $(event.target).text().trim();
        let date = ConvertStringToDate(dateAsString);

        if (dateAsString != $('#calendar-date').text()) {

            $.ajax({
                url: '/Matches/GetByDate',
                method: 'GET',
                data: { sportId: 1, date: date.toLocaleDateString("en-US") },
                success: (resultHtml) => {
                    $('#date-dropdown').css('display', 'none');
                    $('#calendar-date').text(dateAsString);
                    $('#calendar-date').trigger('changeManageDateButtons');
                    $('#partial-body-content').html(resultHtml);
                }
            })
        } else {
            $('#date-dropdown').css('display', 'none');
        }
    })

    $('#right-arrow').click(function () {
        let date = $('#calendar-date').text().trim();
        let resultDate = ConvertStringToDate(date);
        resultDate.setDate(resultDate.getDate() + 1);

        $.ajax({
            url: '/Matches/GetByDate',
            method: 'GET',
            data: { sportId: 1, date: resultDate.toLocaleDateString('en-US') },
            success: (resultHtml) => {
                let oneBeforeLatestDate = $('#calendar-date').data('oneBeforeLatestDate');
                let oneBeforeEarliestDate = $('#calendar-date').data('oneBeforeEarliestDate');

                if (date == oneBeforeLatestDate) {
                    $(this).css('display', 'none');
                }
                else if (date == oneBeforeEarliestDate) {
                    $('#left-arrow').css('display', 'flex');
                }

                let formattedDate = GetFormattedDate(resultDate);

                $('#calendar-date').text(`${formattedDate.day}/${formattedDate.month} ${formattedDate.weekday}`);
                $('#calendar-date').trigger('changeManageDateButtons');
                $('#partial-body-content').html(resultHtml);
            }
        })
    })

    $('#left-arrow').click(function () {
        let date = $('#calendar-date').text().trim();
        let resultDate = ConvertStringToDate(date);
        resultDate.setDate(resultDate.getDate() - 1);

        $.ajax({
            url: '/Matches/GetByDate',
            method: 'GET',
            data: { sportId: 1, date: resultDate.toLocaleDateString('en-US') },
            success: (resultHtml) => {
                let oneBeforeEarliestDate = $('#calendar-date').data('oneBeforeEarliestDate');
                let oneBeforeLatestDate = $('#calendar-date').data('oneBeforeLatestDate');

                if (date == oneBeforeEarliestDate) {
                    $(this).css('display', 'none');
                }
                else if (date == oneBeforeLatestDate) {
                    $('#right-arrow').css('display', 'flex');
                }

                let formattedDate = GetFormattedDate(resultDate);

                $('#calendar-date').text(`${formattedDate.day}/${formattedDate.month} ${formattedDate.weekday}`);
                $('#calendar-date').trigger('changeManageDateButtons');
                $('#partial-body-content').html(resultHtml);
            }
        })
    })

    $('#calendar-div').click('click', () => {
        if ($('#date-dropdown').css('display') == 'none') {
            $('#date-dropdown').css('display', 'block');
        }
        else {
            $('#date-dropdown').css('display', 'none');
        }
    })

    function GetFormattedDate(dateToFormat) {
        let day = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(dateToFormat);
        let month = new Intl.DateTimeFormat('en', { month: '2-digit' }).format(dateToFormat);
        let weekday = new Intl.DateTimeFormat('en', { weekday: 'short' }).format(dateToFormat);

        return { day: day, month: month, weekday: weekday };
    }
}

function ManageUserFavorites() {
    $('.league-star-div').click(function (event) {

        let leagueStar = $(event.target);
        let leagueId = leagueStar.attr('id');
        let topOffset = +leagueStar.offset().top + 10;
        let leftOffset = leagueStar.offset().left;

        $('.column-special-container-manage-league').css({
            'display': 'flex',
            'left': leftOffset + 'px',
            'top': topOffset + 'px'
        });

        if (leagueStar.attr('src') == '/imges/full_star.png') {

            $('#manage-league').attr('class', 'remove-league');
            $('.manage-league-text').text('Remove this league from My Leagues');

            let leagueMatchesCount = $(`#${leagueId}.match-star-img`).filter(function (index, element) {
                return $(element).attr('src') == "/imges/full_star.png";
            });

            if (leagueMatchesCount.length > 0) {
                $('#manage-league-matches').attr('class', 'remove-league-matches');
                $('.manage-league-matches-text').text('Remove all games from Favorites');
            }
            else {
                $('#manage-leagues-matches').attr('class', 'add-league-matches');
                $('.manage-league-matches-text').text('Add all games to Favorites');
            }
        }
        else {
            $('#manage-league').attr('class', 'add-league');
            $('.manage-league-text').text('Add this league to My Leagues');

            let leagueMatchesCount = $(`#${leagueId}.match-star-img`).filter(function (index, element) {
                return $(element).attr('src') == "/imges/full_star.png";
            });

            if (leagueMatchesCount.length > 0) {
                $('#manage-league-matches').attr('class', 'remove-league-matches');
                $('.manage-league-matches-text').text('Remove all games from Favorites');
            }
            else {
                $('#manage-league-matches').attr('class', 'add-league-matches');
                $('.manage-league-matches-text').text('Add all games to Favorites');
            }
        }

        $('#manage-league').children().attr('id', leagueId);
        $('#manage-league-matches').children().attr('id', leagueId);
    });

    $('.league-star-div').one('mouseup', function () {

        $('#manage-league').unbind('click')
        $('#manage-league-matches').unbind('click');

        $('#manage-league').click(function (event) {
            if ($(this).attr('class') == 'remove-league') {

                let id = $(event.target).attr('id');

                $.ajax({
                    method: "POST",
                    url: "/Account/RemoveLeagueFromFavorites",
                    data: { leagueId: id },
                    success: () => {
                        let league = $(`#${id}.flex-row-league`);
                        league.remove();
                        $(`#${id}.league-star-img`).attr('src', '/imges/empty_star.png');
                        $('#manage-league').attr('class', 'add-league');
                        $('.column-special-container-manage-league').css('display', 'none');
                    }
                })
            }
            else {
                let id = $(event.target).attr('id');
                $.ajax({
                    method: 'POST',
                    url: '/Account/AddLeagueToFavorites',
                    data: { leagueId: id },
                    dataType: 'json',
                    success: function (league) {
                        let leagueStar = $(`#${id}.league-star-img`);
                        leagueStar.attr('src', '/imges/full_star.png');
                        $('#user-favorite-leagues').append(`<div id="${id}" class="flex-row-league">
                            <a id="${id}">
                                <div class="special-container" id="${id}">
                                    <div id="${league.id}">
                                        <img class="country-flag" id="${id}" src="${league.country.flagPhotoUrl}">
                                    </div>
                                    <div id="${id}">
                                        <span id="${id}">${league.name}</span>
                                    </div>
                                    <div id="${id}" class="cross">
                                        <img id="${id}" class="cross-img" src="/imges/cross.png" />
                                    </div>
                                </div>
                            </a>
                        </div>`)
                        $('#manage-league').attr('class', 'remove-league');
                        $('.column-special-container-manage-league').css('display', 'none');
                    },
                    error: function (xhr, stat8s, error) {
                        var errorMessage = xhr.status + ': ' + xhr.statusText
                        alert('Error - ' + errorMessage);
                    }
                })
            }
        })
        $('#manage-league-matches').click(function (event) {
            if ($(this).attr('class') == 'remove-league-matches') {
                let id = $(event.target).attr('id');
                let splittedDate = $('#calendar-date').text().split('/').join(' ').split(' ');
                let year = new Date().getFullYear();
                let date = new Date(`${year}/${splittedDate[1]}/${splittedDate[0]}`);
                let parsedDate = date.toLocaleDateString("en-US");

                $.ajax({
                    method: "POST",
                    url: "/Account/RemoveAllLeagueMatchesFromFavorites",
                    data: { leagueId: id, date: parsedDate },
                    success: () => {
                        $('.column-special-container-manage-league').css('display', 'none');
                        let leagueMatchesStar = $(`#${id}.match-star-img`);
                        leagueMatchesStar.attr('src', '/imges/empty_star.png');
                        let leagueMatchesCount = leagueMatchesStar.length;
                        let favoriteMatchesCount = +$('#favoritesDot').text() - leagueMatchesCount;
                        $('#favoritesDot').text(favoriteMatchesCount);
                    }
                })
            }
            else {
                let id = $(event.target).attr('id');
                let dateAsString = $('#calendar-date').text();
                let date = ConvertStringToDate(dateAsString);

                $.ajax({
                    method: "POST",
                    url: "/Account/AddAllLeagueMatchesToFavorites",
                    data: {
                        leagueId: id, date: date.toLocaleDateString("en-US")
                    },
                    success: () => {
                        $('.column-special-container-manage-league').css('display', 'none');
                        let leagueMatchesStar = $(`#${id}.match-star-img`);
                        leagueMatchesStar.attr('src', '/imges/full_star.png');
                        let leagueMatchesCount = leagueMatchesStar.length;
                        let favoriteMatchesCount = +$('#favoritesDot').text() + leagueMatchesCount;
                        $('#favoritesDot').text(favoriteMatchesCount);
                    }
                })
            }

        })
    })

    $('#img-cross-close-manage-league').click(function () {
        $('.column-special-container-manage-league').css('display', 'none');
    });

    $('.cross-img').hover(function () {
        $(this).attr('src', '/imges/darker_cross.png');
    }, function () {
        $(this).attr('src', '/imges/cross.png')
    })

    $('.flex-row-league').hover(function (event) {
        let targetId = $(event.target).attr('id');
        $(`#${targetId}.cross`).css('display', 'block');
    }, function (event) {
        let targetId = $(event.target).attr('id');
        $(`#${targetId}.cross`).css('display', 'none');
    });

    $('.flex-row-league').click(function (event) {
        let id = +$(event.target).attr('id');
        let targetClass = $(event.target).attr('class');

        if (targetClass == 'cross-img' || targetClass == 'cross') {
            $.ajax({
                method: "GET",
                url: "/Account/RemoveLeagueFromFavorites",
                data: { leagueId: id },
                success: function () {
                    let league = $(`#${id}.flex-row-league`);
                    league.remove();
                }
            })
        }
        else {
            $.ajax({
                method: "GET",
                url: "/Leagues/GetById",
                data: { leagueId: id }
            })
        }
    })
}

function ManageMatchesByStatus() {
    $('.get-by-matchstatus-item').click(function () {

        if ($(this).css('background-color') === "rgb(0, 100, 0)") {
            return;
        }

        let matchStatusId = $(this).attr('matchStatusId');
        let sportId = $(this).attr('sportId');
        let stringDate = $('#calendar-date').text();
        let date = ConvertStringToDate(stringDate).toLocaleDateString('en-US');

        $.ajax({
            url: "/Matches/GetByMatchStatusId",
            method: "GET",
            data: { sportId: sportId, matchStatusId: matchStatusId, date: date },
            success: (resultHtml) => {
                ChangeManageDateButtonsOnClick(this);
                $('#partial-body-content').replaceWith(resultHtml);
            }
        })
    })
}

function ManageMatchesBySport() {
    $('#more-a').click(() => {
        let img = $('#more-sports-image')

        if (img.attr('src') == '/imges/triangle_down.png') {
            img.attr('src', '/imges/triangle_up.png');
        }
        else {
            img.attr('src', '/imges/triangle_down.png');
        }
    });
}