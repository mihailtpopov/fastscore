﻿using Data.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestProject.ViewModels;
using TestProject.ViewModels.RoleVMs;

namespace TestProject.Mapping
{
    public class InputMapper
    {
        public static User Map(UserRegisterViewModel userRegisterVM)
        {
            return new User()
            {
                UserName = userRegisterVM.UserName
            };
        }

        public static Role Map(CreateRoleViewModel createRoleVM)
        {
            return new Role()
            {
                Name = createRoleVM.RoleName
            };
        }
    }
}
