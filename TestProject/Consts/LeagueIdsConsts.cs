﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Consts
{
    public static class LeagueIdsConsts
    {
        public const int PREMIER_LEAGUE_ID = 1;
        public const int LA_LIGA_ID = 2;
        public const int BUNDESLIGA_ID = 3;
    }
}
