﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Consts
{
    public static class MatchStatusIdsConsts
    {
        public const int SHEDULED = 1;
        public const int IN_PLAY = 2;
        public const int FINISHED = 3;
    }
}
