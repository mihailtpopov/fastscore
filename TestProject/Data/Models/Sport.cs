﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Data.Models
{
    public class Sport
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public IList<Country> Countries { get; set; }

        public IList<League> Leagues { get; set; }

        public bool IsFavoriteSport { get; set; }

        public string PhotoUrl { get; set; }
    }
}
