﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Models
{
    public class User : IdentityUser
    {
        public string Country { get; set; }

        public IList<Team> MyTeams { get; set; }

        public List<Match> FavoriteMatches { get; set; }

        public List<League> Leagues { get; set; }
    }
}
