﻿using Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.ModelsConfig
{
    class TeamConfigs : IEntityTypeConfiguration<Team>
    {
        public void Configure(EntityTypeBuilder<Team> builder)
        {
            builder.HasMany(t => t.HomeMatches)
                 .WithOne(m => m.HomeTeam)
                 .HasForeignKey(m => m.HomeTeamId)
                 .OnDelete(DeleteBehavior.Restrict);

            builder.HasMany(t => t.AwayMatches)
                .WithOne(m => m.AwayTeam)
                .HasForeignKey(m => m.AwayTeamId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasMany(t => t.ArchiveAwayMatches)
                .WithOne(m => m.AwayTeam)
                .HasForeignKey(m => m.AwayTeamId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasMany(t => t.ArchiveHomeMatches)
                .WithOne(m => m.HomeTeam)
                .HasForeignKey(m => m.HomeTeamId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasMany(t => t.Wins).WithOne(m => m.Winner);
            builder.HasMany(t => t.Losses).WithOne(m => m.Losser);

            builder.HasMany(t => t.ArchiveWins).WithOne(m => m.Winner);
            builder.HasMany(t => t.ArchiveLosses).WithOne(m => m.Losser);
        }
    }
}
