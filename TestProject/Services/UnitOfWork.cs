﻿using Data;
using Services.Contracts;
using Services.Services;
using System;
using System.Threading.Tasks;

namespace Services
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly FastScoreContext context;
        private RoleService roleService;
        private AccountService accountService;
        private SportsService sportsService;
        private MatchesService matchesService;
        public UnitOfWork(FastScoreContext context)
        {
            this.context = context;
        }

        public RoleService RoleService
        {
            get
            {
                if (this.roleService == null)
                {
                    this.roleService = new RoleService(this.context);
                }

                return this.roleService;
            }
        }

        public AccountService AccountService
        {
            get
            {
                if (this.accountService == null)
                {
                    this.accountService = new AccountService(this.context);
                }

                return this.accountService;
            }
        }

        public SportsService SportsService
        {
            get
            {
                if (this.sportsService == null)
                {
                    this.sportsService = new SportsService(this.context);
                }

                return this.sportsService;
            }
        }

        public MatchesService MatchesService
        {
            get
            {
                if (this.matchesService == null)
                {
                    this.matchesService = new MatchesService(this.context);
                }

                return this.matchesService;
            }
        }

        public async Task SaveChangesAsync()
        {
            try
            {
                await this.context.SaveChangesAsync();
            }
            catch(Exception e)
            {
                throw new InvalidOperationException(e.InnerException.Message);
            }
        }
    }
}
