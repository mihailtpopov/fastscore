﻿using Data;
using Data.Models;
using Data.Models.FilteringModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services.Services
{
    public class AccountService
    {
        private readonly FastScoreContext context;

        public AccountService(FastScoreContext context)
        {
            this.context = context;
        }

        public async Task<User> GetByIdAsync(string id)
        {
            return await this.context.Users
                .Include(u => u.FavoriteMatches)
                .Include(u => u.Leagues)
                .FirstOrDefaultAsync(u => u.Id == id);
        }
        public async Task<IEnumerable<User>> SearchByAsync(UserFilteringCriterias userFilteringCriterias)
        {
            if (userFilteringCriterias == null)
            {
                throw new ArgumentNullException();
            }

            IQueryable<User> filteredUsers = this.context.Users;

            if (userFilteringCriterias.UserName != null)
            {
                filteredUsers = filteredUsers.Where(u => u.UserName.StartsWith(userFilteringCriterias.UserName));
            }

            if (userFilteringCriterias.Email != null)
            {
                filteredUsers = filteredUsers.Where(u => u.Email.StartsWith(userFilteringCriterias.Email));
            }

            if (!filteredUsers.Any())
            {
                return new List<User>();
            }

            return await filteredUsers.ToListAsync();
        }

        public async Task AddMatchToFavoritesAsync(int matchId, string userId)
        {
            var match = await this.context.Matches.FirstOrDefaultAsync(m => m.Id == matchId);

            if (match == null)
            {
                throw new ArgumentNullException();
            }

            var user = await this.context.Users.Include(u => u.FavoriteMatches).FirstOrDefaultAsync(u => u.Id == userId);

            if (user == null)
            {
                throw new ArgumentNullException();
            }

            user.FavoriteMatches.Add(match);
            await this.context.SaveChangesAsync();
        }

        public async Task RemoveMatchFromFavoritesAsync(int matchId, string userId)
        {
            var match = await this.context.Matches.FirstOrDefaultAsync(m => m.Id == matchId);

            if (match == null)
            {
                throw new ArgumentNullException();
            }

            var user = await this.context.Users.Include(u => u.FavoriteMatches).FirstOrDefaultAsync(u => u.Id == userId);

            if (user == null)
            {
                throw new ArgumentNullException();
            }

            user.FavoriteMatches.Remove(match);
            await this.context.SaveChangesAsync();
        }

        public async Task RemoveLeagueFromFavoritesAsync(int leagueId, string userId)
        {
            var user = await this.context.Users.Include(u => u.Leagues).FirstOrDefaultAsync(u => u.Id == userId);

            if (user == null)
            {
                throw new ArgumentNullException();
            }

            var league = await this.context.Leagues.FindAsync(leagueId);

            if (league == null)
            {
                throw new ArgumentNullException();
            }

            user.Leagues.Remove(league);
            await this.context.SaveChangesAsync();
        }

        public async Task<League> AddLeagueToFavoritesAsync(int leagueId, string userId)
        {
            var user = await this.context.Users.Include(u=>u.Leagues).FirstOrDefaultAsync(u=>u.Id == userId);

            if (user == null)
            {
                throw new ArgumentNullException();
            }

            var league = await this.context.Leagues.Include(l => l.Country).FirstOrDefaultAsync(l => l.Id == leagueId);

            if (league == null)
            {
                throw new ArgumentNullException();
            }

            user.Leagues.Add(league);
            await this.context.SaveChangesAsync();

            return league;
        }

        public async Task AddAllLeagueMatchesToFavorites(int leagueId, string userId, DateTime date)
        {
            var user = await this.context.Users.Include(u=>u.FavoriteMatches).FirstOrDefaultAsync(u=>u.Id == userId);

            if(user == null)
            {
                throw new ArgumentNullException();
            }

            var league = await this.context.Leagues.Include(l=>l.Matches).FirstOrDefaultAsync(l => l.Id == leagueId);

            if (league == null)
            {
                throw new ArgumentNullException();
            }

            var matches = league.Matches.Where(m => m.StartingTime.Date == date).ToList();
            user.FavoriteMatches.AddRange(matches);
            await this.context.SaveChangesAsync();
        }

        public async Task RemoveAllLeagueMatchesFromFavorites(int leagueId, string userId, DateTime date)
        {
            var user = await this.context.Users.Include(u => u.FavoriteMatches).FirstOrDefaultAsync(u => u.Id == userId);

            if (user == null)
            {
                throw new ArgumentNullException();
            }

            var league = await this.context.Leagues.Include(l => l.Matches).FirstOrDefaultAsync(l => l.Id == leagueId);

            if (league == null)
            {
                throw new ArgumentNullException();
            }

            var matches = league.Matches.Where(m => m.StartingTime.Date == date).ToList();
            user.FavoriteMatches.RemoveAll(m => matches.Contains(m));
            await this.context.SaveChangesAsync();
        }
    }
}
