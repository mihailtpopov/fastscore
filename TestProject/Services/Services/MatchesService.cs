﻿using Consts;
using Data;
using Data.Models;
using Data.Models.Abstracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Services.Services
{
    public class MatchesService
    {
        private readonly FastScoreContext context;

        public MatchesService(FastScoreContext context)
        {
            this.context = context;
        }

        public async Task<Sport> GetByIdAsync(int sportId)
        {
            var sport = await this.context.Sports.FindAsync(sportId);
            return sport;
        }

        public async Task<IDictionary<League, IEnumerable<Match>>> GetByDate(int sportId, DateTime date)
        {
            if ((await this.context.Sports.FindAsync(sportId)) == null)
            {
                throw new ArgumentNullException();
            }

            IDictionary<League, IEnumerable<Match>> dateMatches = null;

            if (date > DateTime.Today)
            {
                 dateMatches = await this.GetLeaguesWithIncludes()
                   .Where(l => l.SportId == sportId && l.Matches.Any(m => m.StartingTime.Date == date))
                   .Select(l => new KeyValuePair<League, IEnumerable<Match>>(l, l.Matches.Where(m => m.StartingTime.Date == date)))
                   .ToDictionaryAsync(kvp => kvp.Key, kvp => kvp.Value);
            }
            else
            {
                dateMatches = await this.GetLeaguesWithIncludesHalfs()
                   .Where(l => l.SportId == sportId && l.Matches.Any(m => m.StartingTime.Date == date))
                   .Select(l => new KeyValuePair<League, IEnumerable<Match>>(l, l.Matches.Where(m => m.StartingTime.Date == date)))
                   .ToDictionaryAsync(kvp => kvp.Key, kvp => kvp.Value);
            }

            return dateMatches;
        }

        public async Task AddOutOfDateMatchesToArchive()
        {
            var outOfDateMatches = await this.GetOutOfDateMatches();
            var archiveMatches = outOfDateMatches as IEnumerable<MatchArchive>;
            var some = outOfDateMatches as IEnumerable<Match>;
            this.context.Matches.RemoveRange(some);

            await this.context.MatchesArchive.AddRangeAsync(archiveMatches);

            await context.SaveChangesAsync();
        }

        public int GetTodayMatchesCount(int sportId)
        {
            var todayMatchesCount = this.context.Matches
                .Include(m => m.League)
                .Where(m => m.League.SportId == sportId && m.StartingTime.Date == DateTime.Today)
                .Count();

            return todayMatchesCount;
        }

        public async Task<IDictionary<League, IEnumerable<Match>>> GetByMatchStatusId(int sportId, int matchStatusId, DateTime date)
        {
            if ((await this.context.Sports.FindAsync(sportId)) == null)
            {
                throw new ArgumentNullException();
            }

            if ((await this.context.MatchStatuses.FindAsync(matchStatusId)) == null)
            {
                throw new ArgumentNullException();
            }

            IDictionary<League, IEnumerable<Match>> dateMatchStatusMatches = null;

            switch (matchStatusId)
            {
                case MatchStatusIdsConsts.FINISHED:
                case MatchStatusIdsConsts.IN_PLAY:
                    dateMatchStatusMatches = await this.GetLeaguesWithIncludesHalfs()
                       .Where(l => l.SportId == sportId && l.Matches.Any(m => m.StartingTime.Date == date && m.MatchStatusId == matchStatusId))
                       .Select(l => new KeyValuePair<League, IEnumerable<Match>>(l, l.Matches.Where(m => m.StartingTime.Date == date && m.MatchStatusId == matchStatusId)))
                       .ToDictionaryAsync(p => p.Key, p => p.Value);
                    break;

                case MatchStatusIdsConsts.SHEDULED:
                    dateMatchStatusMatches = await this.GetLeaguesWithIncludes()
                       .Where(l => l.SportId == sportId && l.Matches.Any(m => m.StartingTime.Date == date && m.MatchStatusId == matchStatusId))
                       .Select(l => new KeyValuePair<League, IEnumerable<Match>>(l, l.Matches.Where(m => m.StartingTime.Date == date && m.MatchStatusId == matchStatusId)))
                       .ToDictionaryAsync(p => p.Key, p => p.Value);
                    break;
            }

            return dateMatchStatusMatches;
        }

        private async Task<IEnumerable<BaseMatch>> GetOutOfDateMatches()
        {
            var date = DateTime.Today.AddDays(-7);
            var outOfDateMatches = await context.Matches.Where(m => m.StartingTime < date).ToListAsync();

            return outOfDateMatches;
        }

        private IQueryable<League> GetLeaguesWithIncludes()
        {
            var leagues = this.context.Leagues
           .Include(l => l.Matches)
               .ThenInclude(m => m.HomeTeam)
           .Include(l => l.Matches)
               .ThenInclude(m => m.AwayTeam)
           .Include(l => l.Country);

            return leagues;
        }

        private IQueryable<League> GetLeaguesWithIncludesHalfs()
        {
            var leagues = this.context.Leagues
           .Include(l => l.Matches)
               .ThenInclude(m => m.HomeTeam)
           .Include(l => l.Matches)
               .ThenInclude(m => m.AwayTeam)
           .Include(l => l.Matches)
               .ThenInclude(m => m.FirstHalf)
           .Include(l => l.Matches)
               .ThenInclude(m => m.SecondHalf)
           .Include(l => l.Matches)
               .ThenInclude(m => m.FirstHalf.HomeGoals)
           .Include(l => l.Matches)
                .ThenInclude(m => m.FirstHalf.AwayGoals)
           .Include(l => l.Matches)
                .ThenInclude(m => m.SecondHalf.HomeGoals)
           .Include(l => l.Matches)
                .ThenInclude(m => m.SecondHalf.AwayGoals)
           .Include(l => l.Country);

            return leagues;
        }
    }
}
