﻿using Services.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts
{
    public interface IUnitOfWork
    {
        public RoleService RoleService { get; }

        public AccountService AccountService { get; }

        public SportsService SportsService { get; }

        public MatchesService MatchesService { get; }

        public Task SaveChangesAsync();
    }
}
